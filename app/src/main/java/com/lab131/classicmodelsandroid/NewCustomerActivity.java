package com.lab131.classicmodelsandroid;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class NewCustomerActivity extends AppCompatActivity {

	Button buttonSubmit;
	EditText editTextCustomerName;
	EditText editTextContactLastName;
	EditText editTextCity;
	EditText editTextState;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_customer);
		setupUI();
	}

	private final class CreateCustomer extends AsyncTask<Void, Void, Void> {
		private String customerName;
		private String contactLastName;
		private String city;
		private String state;

		public CreateCustomer(String customerName, String contactLastName, String city, String state) {
			this.customerName = customerName;
			this.contactLastName = contactLastName;
			this.city = city;
			this.state = state;
		}

		@Override
		protected Void doInBackground(Void... voids) {


			/** This is just boiler plate code of sorts,
			 * need to actually implement POST instead of GET
			 * using OkHttp
			 */

			try {
				String url = "http://classicmodels-lab131.rhcloud.com/customers/";
				okhttp3.MediaType HALJSON = okhttp3.MediaType.parse("application/hal+json");
				OkHttpClient client = new OkHttpClient();
				RequestBody body = RequestBody.create(HALJSON,
						"{\n" +
								"\"customerName\":\""+this.customerName+"\",\n" +
								"\"conctactLastName\":\""+this.contactLastName+"\",\n" +
								"\"city\":\""+this.city+"\",\n" +
								"\"state\":\""+this.state+"\"\n" +
								"}"	);
				Request request = new Request.Builder()
						.url(url)
						.post(body)
						.build();
				final Response response = client.newCall(request).execute();
				final String responseBody = response.body().string();
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						editTextCity.setText("");
						editTextContactLastName.setText("");
						editTextCustomerName.setText("");
						editTextState.setText("");
						editTextCustomerName.requestFocus();
						Toast.makeText(getBaseContext(), responseBody, Toast.LENGTH_SHORT).show();
					}
				});

			} catch (Exception e) {
				Log.e("CustomerDetailsActivity", e.getMessage(), e);
			}

			return null;
		}
	}

	private void setupUI() {

		buttonSubmit = (Button) findViewById(R.id.buttonSubmit);
		editTextCustomerName = (EditText) findViewById(R.id.editTextCustomerName);
		editTextContactLastName = (EditText) findViewById(R.id.editTextContactLastName);
		editTextCity = (EditText) findViewById(R.id.editTextCity);
		editTextState = (EditText) findViewById(R.id.editTextState);

		buttonSubmit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				new CreateCustomer(editTextCustomerName.getText().toString(),
						editTextContactLastName.getText().toString(),
						editTextCity.getText().toString(),
						editTextState.getText().toString()).execute();
			}
		});

	}
}
