package com.lab131.classicmodelsandroid;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.List;

/**
 * Created by chadj on 5/4/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustResponse {

	@JsonProperty("_embedded")
	private HashMap embedded;

	public HashMap getEmbedded() {
		return embedded;
	}

	public void setEmbedded(HashMap embedded) {
		this.embedded = embedded;
	}
}
